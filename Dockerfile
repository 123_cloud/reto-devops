FROM alpine AS builder
RUN mkdir -p /home/node/app/node_modules
WORKDIR /home/node/app
RUN apk add --no-cache --update nodejs nodejs-npm
COPY package*.json ./
RUN npm install --quiet

FROM alpine
ARG USER=asure-sky
WORKDIR /home/node/app
RUN apk add --no-cache --update nodejs && adduser -D $USER
USER $USER

COPY --from=builder /home/node/app/node_modules ./node_modules
COPY --chown=${USER}:${USER} . .
EXPOSE 3000
CMD [ "node", "index.js" ]